<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="css">
    </jsp:attribute>
	<jsp:attribute name="script">
        <!-- app -->
        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
        <script src="js/Config.js"></script>
        <script src="js/main.js"></script>
        <script src="js/app/Utils.js"></script>
        <script src="js/app/Register.js"></script>
    </jsp:attribute>
	<jsp:body>
	    <div id="recent-news">
	        <iframe src="/blank.html" id="loginTarget" name="loginTarget"
					style="display: none"></iframe>
	            <div class="container">
	                <div class="infoTab row ">
	                    <div class="col-xs-12 col-sm-12 col-md-12 ">
	                        <form class="blog-content" id="loginForm"
									action="/blank.html" method="post" target="loginTarget">
	                            <div class="blog-info-acc">
	                                <div class="tab-info-acc">Đăng ký tài khoản trực tuyến</div>
	                                <!---Box--->
	                                <div class="content-info-acc">
	                                    <div class="content-info-acc-in">
	                                        <h5>
	                                            <img
														src="images/Icon Apps.png" />Thông tin tài khoản giao
	                                            dịch chứng khoán
	                                        </h5>
	                                        <div class="lineun"></div>
	
	                                        <!--------ColRespon-------->
	                                        <div
													class="col-md-12 col-ms-12 col-xs-12">
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Họ và tên đệm <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="lastname"
																onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))"
																placeholder="Tiếng Việt có dấu" autofocus>
	                                                    <div for="lastname"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Tên <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="firstname"
																onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))" placeholder="">
	                                                    <div for="firstname"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Giới tính <span>*</span>
	                                                        :
	                                                    </label>
	                                                    <div
																class="checkbox">
	                                                        <label
																	class="gender-item"
																	style="width: 50px; margin: 0 24px 0 0;"> <input
																	name="gender" id="gender1"
																	onchange="Register.changeGender($(this))"
																	type="checkbox"> Nam
	                                                        </label> <label
																	class="gender-item"
																	style="width: 50px; margin: 0 24px 0 0;"> <input
																	name="gender" id="gender2"
																	onchange="Register.changeGender($(this))"
																	type="checkbox"> Nữ
	                                                        </label> <label
																	class="gender-item" style="width: 50px;">
	                                                            <input
																	name="gender" id="gender0"
																	onchange="Register.changeGender($(this))"
																	type="checkbox"> Khác
	                                                        </label>
	                                                    </div>
	                                                    <div
																class="error-msg" for="gender">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Ngày sinh <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="dob" onchange="Register.validateDate($(this), 15, 'Bạn chưa đủ tuổi để mở tài khoản')" onkeyup="Register.validateDate($(this), 15, 'Bạn chưa đủ tuổi để mở tài khoản')"
																data-provide="datepicker" placeholder="dd/mm/yyyy"
																data-date-format="dd/mm/yyyy">
	                                                    <div
																for="dob" class="error-msg">Loi nay cu
	                                                        nay</div>
	                                                </div>
	                                            </div>
	
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Số CMND <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="idnumber"
																onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))" placeholder="">
	                                                    <div for="idnumber"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Ngày cấp <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="iddate"
																onchange="Register.validateDate($(this), undefined, 'Ngày cấp không đúng')"
																placeholder="dd/mm/yyyy" data-date-format="dd/mm/yyyy"
																data-provide="datepicker">
	                                                    <div for="iddate"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Nơi cấp <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="idplace"
																onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))" placeholder="">
	                                                    <div for="idplace"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Email <span>*</span>
	                                                        :
	                                                    </label> <input type="email"
																class="form-control" id="email"
																onkeyup="Register.validateEmail($(this))" onchange="Register.validateEmail($(this))"
																placeholder="Email của bạn">
	                                                    <div for="email"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Số di động <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="mobile"
																onkeyup="Register.validatePhone($(this))" placeholder="" onchange="Register.validatePhone($(this))">
	                                                    <div for="mobile"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label for="address">Địa chỉ liên hệ <span>*</span>
	                                                        :
	                                                    </label> <input type="text"
																class="form-control" id="address" placeholder=""
																onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))">
	                                                    <div for="address"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <label
																for="exampleInputEmail1">Tỉnh thành <span>*</span>
	                                                        :
	                                                    </label> <select
																class="selectpicker2" id="province"
																onchange="Register.validateProvince($(this))">
	                                                        <option value=-1>Chọn tỉnh thành</option>
	                                                        <c:forEach
																	items="${province}" var="element">
	                                                            <option
																		value="<c:out value="${element.getPath()}" />">
	                                                                <c:out
																			value="${element.getName()}" />
	                                                            </option>
	                                                        </c:forEach>
	                                                    </select>
	
	                                                    <div for="province"
																class="error-msg">Loi nay cu nay</div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!--------ColRespon-------->
	                                    </div>
	                                </div>
	                                <!---Box-End--->
	                                <!---Box--->
	                                <div class="content-info-acc">
	                                    <div class="content-info-acc-in">
	                                        <h5>
	                                            <img
														src="images/Icon Apps.png" />Thông tin tài khoản giao
	                                            dịch tiền
	                                        </h5>
	                                        <div class="lineun"></div>
	
	                                        <!--------ColRespon-------->
	                                        <div
													class="col-md-12 col-ms-12 col-xs-12">
	                                            <div
														class="col-md-12 col-ms-12 col-xs-12">
	                                                <div class="form-group">
	                                                    <div
																class="checkbox">
	                                                        <label
																	style="float: left; width: 50px; margin: 0 50px 0 0;">
	                                                            <input
																	name="ismoney" type="checkbox" id="isMoneyInput-1"
																	checked="checked"
																	onchange="Register.changeMoneyInput($(this))"
																	onchange="Register.changeCheckbox('isaccount', 1)">
	                                                            Có
	                                                        </label> <label
																	style="float: left; width: 50px;"> <input
																	name="ismoney" type="checkbox" id="isMoneyInput-0"
																	onchange="Register.changeMoneyInput($(this))"
																	onchange="Register.changeCheckbox('isaccount', 0)">
	                                                            Không
	                                                        </label>
	                                                    </div>
	
	                                                </div>
	                                            </div>
	                                            <div
	                                                class="col-md-12 col-ms-12 col-xs-12" style="padding: 7px 0px;"></div>
	                                            <div class="showbank">
	                                                <div
															class="col-md-3 col-ms-3">
	                                                    <div
																class="form-group">
	                                                        <label
																	for="exampleInputEmail1">Số tài khoản <span>*</span>
	                                                            :
	                                                        </label> <input
																	type="text" class="form-control" id="bankaccountnumber"
																	onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))" placeholder="">
	                                                        <div
																	for="bankaccountnumber" class="error-msg">Không
	                                                            đúng định dạng</div>
	                                                    </div>
	                                                </div>
	                                                <div
															class="col-md-3 col-ms-3">
	                                                    <div
																class="form-group">
	                                                        <label
																	for="exampleInputEmail1">Tên chủ tài khoản
	                                                            <span>*</span> :
	                                                        </label> <input
																	type="text" class="form-control"
																	onkeyup="Register.validateName($(this))" onchange="Register.validateName($(this))"
																	id="bankaccountname" placeholder="">
	                                                        <div
																	for="bankaccountname" class="error-msg">Không
	                                                            đúng định dạng</div>
	                                                    </div>
	                                                </div>
	
	                                                <div
															style="width: 100%; clear: left">
	
	                                                    <div
																class="col-md-3 col-ms-3">
	                                                        <div
																	class="form-group">
	                                                            <label
																		for="exampleInputEmail1">Tỉnh thành <span>*</span>
	                                                                :
	                                                            </label> <select
																		class="selectpicker2" id="bankplace"
																		onchange="Register.changeProvince($(this))">
	                                                                <option value=-1>Chọn tỉnh thành</option>
	                                                                <c:forEach items="${province}" var="element">
	                                                                    <option
																				value="<c:out value="${element.getPath()}" />">
	                                                                        <c:out
																					value="${element.getName()}" />
	                                                                    </option>
	                                                                </c:forEach>
	                                                            </select>
	                                                            <div for="bankplace" class="error-msg">Không đúng định dạng</div>
	                                                        </div>
	                                                    </div>
	                                                    <div
																class="col-md-3 col-ms-3">
	                                                        <div
																	class="form-group">
	                                                            <label
																		for="exampleInputEmail1">Tại ngân hàng <span>*</span>
	                                                                :
	                                                            </label> <select
																		class="selectpicker2" id="bankname"
																		onchange="Register.changeBank($(this))">
	                                                                <option
																			value="-1">Chọn ngân hàng</option>
	                                                            </select>
	                                                            <div
																		for="bankname" class="error-msg">Không đúng
	                                                                định dạng</div>
	                                                        </div>
	                                                    </div>
	                                                    <div
																class="col-md-3 col-ms-3">
	                                                        <div
																	class="form-group">
	                                                            <label
																		for="exampleInputEmail1">Chi nhánh <span>*</span>
	                                                                :
	                                                            </label> <select
																		class="selectpicker2" id="bankcode"
																		onchange="Register.validateProvince($(this))">
	                                                                <option
																			value=-1>Chọn chi nhánh</option>
	                                                            </select>
	                                                            <div
																		for="bankcode" class="error-msg">Không đúng
	                                                                định dạng</div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!--------ColRespon-------->
	                                    </div>
	                                </div>
	                                <!---Box-End--->
	                                <!---Box--->
	                                <div class="content-info-acc">
	                                    <div class="content-info-acc-in">
	                                        <h5>
	                                            <img
														src="images/Icon Apps.png" />Xác nhận thông tin:
	                                        </h5>
	                                        <div class="lineun"></div>
	                                        <!--------ColRespon-------->
	                                        <div
													class="col-md-12 col-ms-12 col-xs-12">
	                                            <div
														class="col-md-12 col-ms-12 col-xs-12">
	                                                <div class="form-group">
	                                                    <div
																class="checkbox">
	                                                        <label> <input
																	type="checkbox" id="confirm"
																	onchange="Register.confirm($(this))"> Tôi cam kết
	                                                            những thông tin của khách hàng nêu trên là chính xác
	                                                        </label>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div
														class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <div
																class="g-recaptcha"
																data-sitekey="6LdBIQcTAAAAANP9rplQzyRvJmLKRzsOCYnd-TEp"></div>
	                                                    <div id="captcha"
																class="error-msg">Captcha chưa đúng</div>
	                                                </div>
	                                            </div>
	                                            <!-- <div class="col-md-3 col-ms-3">
	                                                <div class="form-group">
	                                                    <input type="email" class="form-control"
	                                                        id="exampleInputEmail1"
	                                                        placeholder="Nhập lại ký tự hình bên">
	                                                </div>
	                                            </div> -->
	                                            <div
														style="width: 100%; display: block; overflow: hidden; clear: both;">
	                                                <div
															class="col-md-3 col-ms-3">
	                                                    <div
																class="form-group">
	                                                        <input
																	class="btn btn-default btn2" type="submit"
																	value="Đăng ký" onclick="Register.addAccount()">
	                                                    </div>
	                                                </div>
	                                                <div
															class="col-md-3 col-ms-3">
	                                                    <div
																class="form-group">
	                                                        <input
																	class="btn btn-default btn2" type="submit" value="Huỷ">
	                                                    </div>
	                                                </div>
	                                                <div
															class="col-md-3 col-ms-3">
	                                                    <div
																class="form-group">
	                                                        <input
																	class="btn btn-default btn2" type="submit"
																	value="In hồ sơ">
	                                                    </div>
	                                                </div>
	                                                <div
															class="col-md-3 col-ms-3">
	                                                    <div
																class="form-group">
	                                                        <input
																	class="btn btn-default btn2" type="submit"
																	value="Bổ sung thông tin">
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!--------ColRespon-------->
	                                    </div>
	                                </div>
	                                <!---Box-End--->
	                            </div>
	                        </form>
	                    </div>
	                </div>
	                <!--/.row-->
	                <div class="modal fade" id="basicModal" tabindex="-1"
							role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	                    <div class="modal-dialog modal-sm">
	                        <div class="modal-content">
	                            <div class="modal-header">
	                                <h4 class="modal-title"
											id="myModalLabel">Có lỗi</h4>
	                            </div>
	                            <div class="modal-body">
	                                <h3>Vui lòng kiểm tra lại thông tin!</h3>
	                            </div>
	                            <div class="modal-footer">
	                                <button type="button"
											class="btn btn-default" data-dismiss="modal">Đóng lại</button>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                </form>
	            </div>
	            <!--/.container-->
	        </div>
	</jsp:body>
</t:genericpage>