Utils = {
	validateEmail : function(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	},

	validateSpecialCharacter : function(input) {
		var regex = new RegExp('[$&+:;=?@#|]', 'g');
		return regex.test(input);
	},

	validatePhone : function(input) {
		var regex = new RegExp('^(\\+*)\\d{10,12}$', 'g');
		return regex.test(input);
	},

	validateSmallerDate : function(date, year) {
		var d = date.split("/");
		date = d[1] + "/" + d[0] + "/" + d[2];
		dateObj = new Date(date);
		if (!year) {
			return dateObj < new Date();
		} else {
			var obj = new Date();
			var newDate = new Date(obj.getFullYear() - year, obj.getMonth(),
					obj.getDate());
			return dateObj <= newDate;
		}
	},

	trim : function(str) {
		if (!str)
			return str;
		str = str.replace(/^\s+/, '');
		for (var i = str.length - 1; i >= 0; i--) {
			if (/\S/.test(str.charAt(i))) {
				str = str.substring(0, i + 1);
				break;
			}
		}
		return str;
	},

};