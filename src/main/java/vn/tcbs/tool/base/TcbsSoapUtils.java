package vn.tcbs.tool.base;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

public class TcbsSoapUtils {
	public static SOAPConnection getSoapConnection() {
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();
			return soapConnection;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static SOAPMessage generateSoapMessage(String filePath,
			HashMap<String, Object> data, String action) {
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			String soapText = TcbsFileUtils.readResourceFile(filePath);
			String soapContent = TcbsUtils.replaceHolder(soapText, data);
			
			System.out.println("SEND CONTENT: " + soapContent);
			
			SOAPMessage soapMessage = messageFactory.createMessage(
					new MimeHeaders(),
					new ByteArrayInputStream(soapContent.getBytes(Charset
							.forName("UTF-8"))));
			MimeHeaders headers = soapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", action);

			soapMessage.saveChanges();
			return soapMessage;
		} catch (Exception ex) {
			return null;
		}
	}
}
