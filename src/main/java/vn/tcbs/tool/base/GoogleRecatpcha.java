package vn.tcbs.tool.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GoogleRecatpcha {

	public static boolean check(String secret, String response, String remoteip)
			throws ClientProtocolException, IOException {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(
				"https://www.google.com/recaptcha/api/siteverify");
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("secret", secret));
		urlParameters.add(new BasicNameValuePair("response", response));
		urlParameters.add(new BasicNameValuePair("remoteip", remoteip));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse res = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(res
				.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println(result.toString());
		
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> ret = mapper.readValue(result.toString(), HashMap.class);
		if((boolean) ret.get("success") == false){
			return false;
		}
		return true;
	}
}
