package vn.tcbs.website.tcbswebsite.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.tool.base.TcbsUtils;
import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;
import vn.tcbs.website.tcbswebsite.entity.Province;
import vn.tcbs.website.tcbswebsite.object.OpenAccountMessage;
import vn.tcbs.website.tcbswebsite.service.IBankService;
import vn.tcbs.website.tcbswebsite.service.IFSSService;
import vn.tcbs.website.tcbswebsite.service.IOpenAccountService;
import vn.tcbs.website.tcbswebsite.service.IProvinceService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/")
public class HomeController {

	@Autowired
	private IProvinceService provinceService;
	@Autowired
	private IBankService bankService;
	@Autowired
	private IFSSService fssService;
	@Autowired
	private IOpenAccountService openAccountService;

	@RequestMapping(value = "/tai-khoan", method = RequestMethod.GET)
	public ModelAndView dashboard(HttpServletRequest req) {
		return new ModelAndView("account/index");
	}

	@RequestMapping(value = "/rm/login")
	public ModelAndView rmLogin(HttpServletRequest req) {
		req.getSession().setAttribute("agentid", req.getParameter("agentid"));
		String redirect = req.getParameter("redirect");
		redirect = (redirect == null || redirect.equals("")) ? "mo-tai-khoan.html"
				: redirect;
		return new ModelAndView("redirect:/" + redirect);
	}

	@RequestMapping(value = "/rm/logout", method = RequestMethod.GET)
	public ModelAndView rmLogout(HttpServletRequest req) {
		req.getSession().removeAttribute("agentid");
		return new ModelAndView("redirect:/mo-tai-khoan.html");
	}

	@RequestMapping(value = "/xac-thuc-rm", method = RequestMethod.GET)
	public ModelAndView rmValidate(HttpServletRequest req,
			HttpServletResponse res) {

		String agentid = (String) req.getSession().getAttribute("agentid");
		if (agentid != null && !agentid.equals("")) {
			return new ModelAndView("redirect:/mo-tai-khoan.html");
		}
		String redirect = req.getParameter("redirect");
		redirect = (redirect == null || redirect.equals("")) ? "mo-tai-khoan.html"
				: redirect;
		// String redirect = "tai-khoan.html";
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("redirect", redirect);
		return new ModelAndView("rm/validate", model);
	}

	@RequestMapping(value = "/mo-tai-khoan", method = RequestMethod.GET)
	public ModelAndView createNew(HttpServletRequest req,
			HttpServletResponse res) {

		String agentid = (String) req.getSession().getAttribute("agentid");
		if (agentid == null || agentid.equals("")) {
			return new ModelAndView(
					"redirect:/xac-thuc-rm.html?redirect=mo-tai-khoan.html");
		}

		List<Province> province = provinceService.listAll();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("province", province);
		model.put("agentid", req.getAttribute("agentid"));
		return new ModelAndView("account-add", model);
	}

	@RequestMapping(value = "/mo-tai-khoan-excel", method = RequestMethod.GET)
	public ModelAndView openAccountExcel(HttpServletRequest req,
			HttpServletResponse res) {
		Boolean isLoggedIn = TcbsUtils.validateLogin(req);
		if (!isLoggedIn) {
			return new ModelAndView(
					"redirect:/xac-thuc-rm.html?redirect=mo-tai-khoan-excel.html");
		}
		String agentId = (String) req.getSession().getAttribute("agentid");

		HashMap<String, Object> model = new HashMap<String, Object>();
		HttpSession session = req.getSession();
		Integer errMsg = (Integer) session.getAttribute("upload_message");
		if (errMsg != null) {
			model.put("upload_message", errMsg);
			session.removeAttribute("upload_message");
		}

		String excelFile = (String) session.getAttribute("excelfile");

		if (excelFile != null && !excelFile.equals("")) {
			HashMap<String, Integer> err = new HashMap<String, Integer>();
			List<String[]> persons = openAccountService
					.readExcelFile(excelFile);
			for (int j = 0; j < persons.size(); j++) {
				String[] person = persons.get(j);
				OpenAccountMessage msg = new OpenAccountMessage();
				msg.setLastname(person[1]);
				msg.setFirstname(person[2]);
				msg.setSex(person[3].equalsIgnoreCase("m") ? "001" : person[3]
						.equalsIgnoreCase("f") ? "002" : "003");
				msg.setDob(person[4]);
				msg.setIdnumber(person[5]);
				msg.setIddate(person[6]);
				msg.setIdplace(person[7]);
				msg.setEmail(person[8]);
				msg.setMobile(person[9]);
				msg.setAddress(person[10]);
				msg.setProvince(person[11]);

				BindingResult errors = TcbsUtils.validateOpenAccount(msg);
				if (errors.hasErrors()) {
					System.out.println("ERROR " + msg.getIdnumber());
					err.put(msg.getIdnumber(), 1);
				}
				Integer errorCode = fssService.openAccount(msg, agentId);
				if (errorCode != 0) {
					System.out.println("ERROR " + msg.getIdnumber());
					err.put(msg.getIdnumber(), 1);
				}
			}
			model.put("persons", persons);
			model.put("errors", err);
		}
		session.removeAttribute("excelfile");
		return new ModelAndView("account/open-account-excel", model);
	}

	@RequestMapping(value = "/tai-khoan/danh-sach-khach-hang", method = RequestMethod.GET)
	public ModelAndView createAccountSuccess(HttpServletRequest req,
			HttpServletResponse res) {
		String agentid = (String) req.getSession().getAttribute("agentid");
		if (agentid == null || agentid.equals("")) {
			return new ModelAndView(
					"redirect:/xac-thuc-rm.html?redirect=tai-khoan/xac-nhan-mo-tai-khoan.html");
		}
		String statusStr = req.getParameter("status");
		Integer status = 0;
		if (statusStr != null && !statusStr.equals("")) {
			status = Integer.valueOf(statusStr);
		}
		System.out.println("Status: " + status);

		HashMap<String, Object> model = new HashMap<String, Object>();
		List<OpenAccountRequest> nonRegistered = openAccountService.find(
				agentid, null, status);
		List<OpenAccountRequest> registered = openAccountService.find(agentid,
				null, 1);
		model.put("nonRegistered", nonRegistered);
		model.put("registered", registered);
		model.put("status", status);

		return new ModelAndView("account/customer-list", model);
	}

	@RequestMapping(value = "/tai-khoan/xac-nhan-mo-tai-khoan")
	public ModelAndView confirmOpenAccount(HttpServletRequest req,
			HttpServletResponse res) {
		String agentid = (String) req.getSession().getAttribute("agentid");
		if (agentid == null || agentid.equals("")) {
			return new ModelAndView(
					"redirect:/xac-thuc-rm.html?redirect=tai-khoan/xac-nhan-mo-tai-khoan.html");
		}
		HashMap<String, Object> model = new HashMap<String, Object>();
		if (req.getMethod().equalsIgnoreCase("post")) {
			String idnumber = req.getParameter("idnumber");
			Boolean isConfirm = fssService.confirmOpeningAccount(idnumber,
					agentid);
			if (isConfirm) {
				OpenAccountRequest acc = openAccountService
						.getByIdNumberAndAgentAndStatus(idnumber, agentid, 0);
				if (acc != null) {
					openAccountService.changeStatus(acc, 1);
					return new ModelAndView(
							"redirect:/tai-khoan/xac-nhan-mo-tai-khoan.html");
				} else {
					model.put("error",
							"CMND không phải khách hàng của bạn hoặc đã được mở tài khoản");
				}
			} else {
				model.put("error",
						"CMND không phải khách hàng của bạn hoặc đã được mở tài khoản");
			}
		} else {
			String idOpen = req.getParameter("idopen");
			List<OpenAccountRequest> customerList = openAccountService.find(
					agentid, null, 0);
			model.put("customer", customerList);
			if (idOpen != null && !idOpen.equals("")) {
				List<OpenAccountRequest> openUsers = openAccountService.find(
						agentid, idOpen, 0);
				if (openUsers.size() > 0) {
					OpenAccountRequest openUser = openUsers.get(0);
					ObjectMapper mapper = new ObjectMapper();
					try {
						HashMap<String, Object> obj = mapper.readValue(
								openUser.getRequestData(), HashMap.class);
						model.put("address", obj.get("address"));
					} catch (Exception ex) {
					}
					model.put("user", openUser);
				}
			}
			model.put("idopen", idOpen);
		}
		return new ModelAndView("account/confirm", model);
	}

}