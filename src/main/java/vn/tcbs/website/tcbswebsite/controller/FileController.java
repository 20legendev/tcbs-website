package vn.tcbs.website.tcbswebsite.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.cxf.helpers.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsFileUtils;
import vn.tcbs.website.tcbswebsite.entity.Contract;
import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;
import vn.tcbs.website.tcbswebsite.service.IContractService;
import vn.tcbs.website.tcbswebsite.service.IOpenAccountService;

@Controller
@RequestMapping(value = "/file")
public class FileController {

	@Autowired
	private IContractService contractService;
	@Autowired
	private IOpenAccountService openAccountService;

	@Value("${uploader.directory}")
	private String uploadDirectory;

	@RequestMapping(value = "/public/{filename}", method = RequestMethod.GET)
	public ModelAndView downloadPublicFile(
			@PathVariable("filename") String fName, HttpServletRequest req,
			HttpServletResponse response) {
		String filePath = System.getProperty("user.home") + File.separator
				+ "tcbs" + File.separator + "public" + File.separator + fName;
		try {
			InputStream inputStream = new FileInputStream(new File(filePath));
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename="
					+ fName);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
			inputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/download/{alias}", method = RequestMethod.GET)
	public ModelAndView dashboard(@PathVariable("alias") String alias,
			HttpServletRequest req, HttpServletResponse response) {
		Contract contract = contractService.getByAlias(alias);

		if (contract != null) {
			try {
				String idNumber = contract.getIdNumber();
				List<OpenAccountRequest> accRequest = openAccountService.find(
						null, idNumber, null);
				if (accRequest.size() > 0) {
					contract.setDownloadCount(contract.getDownloadCount() + 1);
					contractService.update(contract);
					String downloadName = "TCBS-Contract-"
							+ contract.getIdNumber() + "-"
							+ accRequest.get(0).getFullName() + ".docx";
					String fileName = contract.getDocumentPath();
					InputStream inputStream = new FileInputStream(new File(
							fileName));
					response.setContentType("application/force-download");
					response.setHeader("Content-Disposition",
							"attachment; filename=" + downloadName);
					IOUtils.copy(inputStream, response.getOutputStream());
					response.flushBuffer();
					inputStream.close();
				} else {
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@RequestMapping(value = "/download/tc", method = RequestMethod.GET)
	public ModelAndView downloadTC(HttpServletResponse response) {
		String downloadName = "MB-TCBS-TC01.pdf";
		String fileName = System.getProperty("user.home") + File.separator
				+ "tcbs" + File.separator + "template" + File.separator
				+ "MB-TCBS-TC01.pdf";
		try {
			InputStream inputStream = new FileInputStream(new File(fileName));
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename="
					+ downloadName);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
			inputStream.close();
			return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

}