package vn.tcbs.website.tcbswebsite.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsFileUtils;

@RestController
@RequestMapping(value = "/upload")
public class UploadController {

	@Value("${uploader.directory}")
	private String uploadDirectory;

	@RequestMapping(value = "/excel", method = RequestMethod.POST)
	public ModelAndView uploadExcel(HttpServletRequest request,
			HttpServletResponse response) {
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		String uploadDir = System.getProperty("user.home") + uploadDirectory
				+ File.separator
				+ new SimpleDateFormat("yyyyMMdd").format(new Date())
				+ File.separator;
		File outputF = new File(uploadDir + ".metadata");
		if (!outputF.exists()) {
			outputF.getParentFile().mkdirs();
			outputF.getParentFile().setWritable(true);
			outputF.getParentFile().setReadable(true);
		}
		List<String> listFile = new ArrayList<String>();

		try {
			List<FileItem> multiparts = upload.parseRequest(request);
			for (FileItem item : multiparts) {
				if (!item.isFormField()
						&& TcbsFileUtils.getMimeType(item.getContentType()) == "EXCEL") {
					String filePath = TcbsFileUtils.generateFilename(uploadDir,
							item.getName());
					item.write(new File(filePath));
					listFile.add(filePath);
				}
			}
			request.getSession().setAttribute("excelfile", listFile.get(0));
			request.getSession().setAttribute("upload_message", 0);
			return new ModelAndView("redirect:/mo-tai-khoan-excel.html");
		} catch (Exception e) {
			request.getSession().setAttribute("upload_message", 1);
			return new ModelAndView("redirect:/mo-tai-khoan-excel.html");
		}
	}
}