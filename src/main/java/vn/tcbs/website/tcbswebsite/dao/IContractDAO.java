package vn.tcbs.website.tcbswebsite.dao;

import java.util.List;

import vn.tcbs.website.tcbswebsite.entity.Contract;

public interface IContractDAO {

	public List<Contract> getByAlias(String alias);

	public Contract createNew(Contract c);

	public Contract checkContract(String contractNumber);

	public List<Contract> getByIdNumber(String idNumber);

	public Contract update(Contract c);
}
