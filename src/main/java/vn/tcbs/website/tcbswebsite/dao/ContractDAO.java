package vn.tcbs.website.tcbswebsite.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.base.TcbsUtils;
import vn.tcbs.website.tcbswebsite.entity.Contract;

@Repository("contractDAO")
@Transactional
public class ContractDAO extends BaseDAO implements IContractDAO {

	@Override
	public List<Contract> getByAlias(String alias) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("documentAlias", alias));
		return cr.list();
	}

	@Override
	public Contract createNew(Contract c) {
		c.setContractNumber(TcbsUtils.generateContractId(this));
		c.setDocumentAlias(TcbsUtils.generateRandom(24));
		c.setCreatedDate(new Date());
		getSession().saveOrUpdate(c);
		return c;
	}

	@Override
	public Contract checkContract(String contractNumber) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("contractNumber", contractNumber));
		List<Contract> result = cr.list();
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public List<Contract> getByIdNumber(String idNumber) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("idNumber", idNumber));
		return cr.list();
	}

	@Override
	public Contract update(Contract c) {
		getSession().update(c);
		return c;
	}
}
