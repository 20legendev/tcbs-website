package vn.tcbs.website.tcbswebsite.dao;

import java.util.List;

import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;

public interface IOpenAccountRequestDAO {
	public void addNew(String fullName, String idNumber, String agentId,
			String referenceId, String requestData);

	public void update(OpenAccountRequest oar);
	
	public List<OpenAccountRequest> getByIdNumberAndAgentAndStatus(
			String agentId, String idnumber, Integer status);

	public void changeStatus(OpenAccountRequest obj, Integer status);

	public List<OpenAccountRequest> find(String agentId, String idnumber,
			Integer status);
}
