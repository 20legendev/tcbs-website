package vn.tcbs.website.tcbswebsite.dao;

import java.util.List;

import vn.tcbs.website.tcbswebsite.entity.Province;

public interface IProvinceDAO {
	public List<Province> listAll();

	public List<Province> getByPath(String path);
}
