package vn.tcbs.website.tcbswebsite.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;

@Repository("openAccountDAO")
@Transactional
public class OpenAccountRequestDAO extends BaseDAO implements
		IOpenAccountRequestDAO {

	@Override
	public void addNew(String fullName, String idNumber, String agentId,
			String referenceId, String requestData) {
		// TODO transaction confirm to make sure data saved to database
		// successfully
		OpenAccountRequest acc = new OpenAccountRequest();
		acc.setFullName(fullName);
		acc.setIdNumber(idNumber);
		acc.setAgentId(agentId);
		acc.setReferenceId(referenceId);
		acc.setRequestData(requestData);
		acc.setStatus(0);
		acc.setLastUpdated(new Date());
		getSession().save(acc);
	}

	@Override
	public List<OpenAccountRequest> find(String agentId, String idNumber,
			Integer status) {
		String qStr = "from OpenAccountRequest where 1=1 ";
		if (agentId != null) {
			qStr += " and agentId = :agentId";
		}
		if (idNumber != null) {
			qStr += " and idNumber = :idNumber";
		}
		if (status != null) {
			qStr += " and status = :status";
		}
		qStr += " order by lastUpdated desc";
		Query q = getSession().createQuery(qStr);
		if (agentId != null) {
			q.setParameter("agentId", agentId);
		}
		if (idNumber != null) {
			q.setParameter("idNumber", idNumber);
		}
		if (status != null) {
			q.setParameter("status", status);
		}
		return q.list();
	}

	@Override
	public List<OpenAccountRequest> getByIdNumberAndAgentAndStatus(
			String agentId, String idnumber, Integer status) {
		Query q = getSession()
				.createQuery(
						"from OpenAccountRequest where idNumber = ? and agentId = ? and status = ? order by lastUpdated desc");
		q.setParameter(0, idnumber).setParameter(1, agentId)
				.setParameter(2, status);
		return q.list();
	}

	@Override
	public void changeStatus(OpenAccountRequest obj, Integer status) {
		obj.setStatus(status);
		getSession().update(obj);
	}

	@Override
	public void update(OpenAccountRequest oar) {
		getSession().update(oar);
	}
}
