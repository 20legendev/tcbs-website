package vn.tcbs.website.tcbswebsite.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.website.tcbswebsite.entity.Province;

@Repository("provinceDAO")
@Transactional
public class ProvinceDAO extends BaseDAO implements IProvinceDAO {

	@Override
	@Cacheable(value = "shortCache", key = "#root.method.name")
	public List<Province> listAll() {
		Query q = getSession().createQuery(
				"from Province order by provinceOrder desc, name asc");
		return q.list();
	}

	@Override
	@Cacheable(value = "shortCache", key = "#root.method.name + #path")
	public List<Province> getByPath(String path) {
		// TODO Auto-generated method stub
		Query q = getSession().createQuery("from Province where path = ?");
		q.setParameter(0, path);
		return q.list();
	}

}
