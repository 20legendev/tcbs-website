package vn.tcbs.website.tcbswebsite.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import vn.tcbs.ebs.doc.util.DocReporterUtil;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsUtils;
import vn.tcbs.website.tcbswebsite.entity.Contract;
import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;
import vn.tcbs.website.tcbswebsite.entity.Province;
import vn.tcbs.website.tcbswebsite.object.OpenAccountMessage;
import vn.tcbs.website.tcbswebsite.service.IBankService;
import vn.tcbs.website.tcbswebsite.service.IContractService;
import vn.tcbs.website.tcbswebsite.service.IFSSService;
import vn.tcbs.website.tcbswebsite.service.IOpenAccountService;
import vn.tcbs.website.tcbswebsite.service.IProvinceService;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

@WebService
@Path(value = "/account")
public class AccountWebService implements IAccountWebService {

	@Autowired
	private IOpenAccountService openAccountService;
	@Autowired
	private IFSSService fssService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IProvinceService provinceService;
	@Autowired
	private IBankService bankService;

	private String serverURI = "http://tempuri.org/";
	private String service = "http://113.190.244.46:8383/OnlineTradingWcf.svc?wsdl";

	@Override
	@POST
	@Path("/open")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response createNew(@FormParam("") OpenAccountMessage msg,
			@Context HttpServletRequest request) {
		Boolean isLoggedIn = TcbsUtils.validateLogin(request);
		if (!isLoggedIn) {
			return Response
					.ok(new ResponseData<String>(4, "", "CAPTCHA_ERROR"))
					.header("Access-Control-Allow-Origin", "*").build();
		}

		String agentId = (String) request.getSession(true).getAttribute(
				"agentid");

		BindingResult errors = TcbsUtils.validateOpenAccount(msg);
		if (errors.hasErrors()) {
			return Response
					.ok(new ResponseData<List<ObjectError>>(3, errors
							.getAllErrors(), "ERROR"))
					.header("Access-Control-Allow-Origin", "*").build();
		}

		ResponseBuilder res;
		Integer errorCode = fssService.openAccount(msg, agentId);
		switch (errorCode) {
		case 0:
			res = Response.ok(new ResponseData<String>(0, null, "SUCCESS"));
			break;
		case -100098:
			res = Response.ok(new ResponseData<Integer>(1, errorCode,
					"ID_DUPLICATED"));
			break;
		case -100054:
			res = Response.ok(new ResponseData<Integer>(1, errorCode,
					"NOT_AUTHORIZED"));
			break;
		default:
			res = Response.ok(new ResponseData<Integer>(2, errorCode, "ERROR"));
			break;
		}

		return res.header("Access-Control-Allow-Origin", "*").build();
	}

	@Override
	@POST
	@Path("/check-id")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response checkIdNumber(@FormParam("idnumber") String idnumber,
			@Context HttpServletRequest request) {
		String agentid = (String) request.getSession().getAttribute("agentid");
		OpenAccountRequest acc = openAccountService
				.getByIdNumberAndAgentAndStatus(idnumber, agentid, 0);
		return Response.ok(
				new ResponseData<OpenAccountRequest>(0, acc, "SUCCESS"))
				.build();
	}

	@Override
	@POST
	@Path("/cancel")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response cancelOpenAccount(@FormParam("idnumber") String idnumber,
			@Context HttpServletRequest request) {
		String agentid = (String) request.getSession().getAttribute("agentid");
		OpenAccountRequest acc = openAccountService
				.getByIdNumberAndAgentAndStatus(idnumber, agentid, 0);

		ResponseData resData;
		if (acc != null) {
			if (fssService.cancelOpenAccount(acc.getReferenceId())) {
				openAccountService.changeStatus(acc, 2);
				resData = new ResponseData<String>(0, "", "SUCCESS");
			} else {
				resData = new ResponseData<String>(1, "", "ERROR");
			}
		} else {
			resData = new ResponseData<String>(2, "", "NO_DATA");
		}
		return Response.ok(resData).build();
	}

	@POST
	@Path("/convert")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response printDocument(@FormParam("idnumber") String idNumber,
			@Context HttpServletRequest request) {
		ResponseData responseData = null;
		String agentid = (String) request.getSession().getAttribute("agentid");
		List<OpenAccountRequest> listAcc = openAccountService.find(null,
				idNumber, null);
		if (listAcc.size() == 0) {
			responseData = new ResponseData<String>(1, "", "NOT_FOUND");
		} else {
			OpenAccountRequest acc = listAcc.get(0);
			String folder = System.getProperty("user.home") + File.separator
					+ "tcbs" + File.separator;
			String path = folder + "template" + File.separator
					+ "MB-TCBS-HD01.docx";
			String output = folder + "generate" + File.separator
					+ new SimpleDateFormat("yyyyMMdd").format(new Date())
					+ File.separator + "MB-TCBS-HD01-"
					+ new Random().nextInt((10000) + 1) + ".docx";

			File templateFile = new File(path);
			InputStream in;
			try {
				HashMap<String, Object> accData = new ObjectMapper().readValue(
						acc.getRequestData(), HashMap.class);

				String tmp = (String) accData.get("sex");
				accData.put("gender", tmp.equals("001") ? "Nam" : "Nữ");

				Province province = provinceService.getByPath((String) accData
						.get("province"));
				accData.put("province_text",
						province != null ? province.getName()
								: (String) accData.get("province"));
				String bankCode = (String) accData.get("bankcode");

				System.out.println("ID number: "
						+ (String) accData.get("idnumber"));

				Contract contract = contractService
						.getByIdNunber((String) accData.get("idnumber"));
				if (contract != null) {
					accData.put("contractnumber", contract.getContractNumber());
				} else {
					System.out.println("Account null");
				}

				if (bankCode != null && !bankCode.equals("")) {
					HashMap<String, Object> bank = bankService
							.getByBankCode(bankCode);
					if (bank != null) {
						accData.put("bankcode_text", bank.get("branchname")
								+ ", " + bank.get("regional"));
					}
				} else {
					accData.put("bankcode_text", "");
				}

				in = new FileInputStream(templateFile);
				IXDocReport report = XDocReportRegistry.getRegistry()
						.loadReport(in, TemplateEngineKind.Velocity);
				IContext context = report.createContext();
				context.putMap(accData);
				DocReporterUtil
						.processDocumentReportDocx(path, output, context);

				contract.setDocumentPath(output);
				contractService.updateByIdNumber(contract);
				responseData = new ResponseData<String>(0,
						contract.getDocumentAlias(), "SUCCESS");

			} catch (Exception e) {
				responseData = new ResponseData<String>(2, "", "DATA_ERROR");
				e.printStackTrace();
			}
		}
		return Response.ok(responseData).build();
	}

	@Override
	@POST
	@Path("/open-batch")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response openBatch(@Context HttpServletRequest req) {
		ResponseData<HashMap> res = null;
		String agentId = (String) req.getSession().getAttribute("agentid");
		Boolean isLoggedIn = TcbsUtils.validateLogin(req);
		if (!isLoggedIn) {
			res = new ResponseData<HashMap>(1, null, "NOT_AUTHORIZED");
		} else {
			List<String> listFile = new ArrayList<String>();
			listFile.add("/Users/kiennt/Documents/TCBS-OpenAccount.xlsx");
			HashMap<String, Object> model = new HashMap<String, Object>();
			String[] person = null;
			HashMap<String, Integer> err = new HashMap<String, Integer>();

			if (listFile != null) {
				String filePath = listFile.get(0);
				List<String[]> persons = openAccountService
						.readExcelFile(filePath);
				for (int j = 0; j < persons.size(); j++) {
					person = persons.get(j);
					OpenAccountMessage msg = new OpenAccountMessage();
					msg.setLastname(person[1]);
					msg.setFirstname(person[2]);
					msg.setSex(person[3].equalsIgnoreCase("m") ? "001"
							: person[3].equalsIgnoreCase("f") ? "002" : "003");
					msg.setDob(person[4]);
					msg.setIdnumber(person[5]);
					msg.setIddate(person[6]);
					msg.setIdplace(person[7]);
					msg.setEmail(person[8]);
					msg.setMobile(person[9]);
					msg.setAddress(person[10]);
					msg.setProvince(person[11]);

					BindingResult errors = TcbsUtils.validateOpenAccount(msg);
					if (errors.hasErrors()) {
						err.put(msg.getIdnumber(), 1);
					}
					Integer errorCode = fssService.openAccount(msg, agentId);
					if (errorCode != 0) {
						err.put(msg.getIdnumber(), 1);
					}
				}
				model.put("persons", persons);
				model.put("errors", err);
			}
			res = new ResponseData<HashMap>(0, model, "SUCCESS");
		}
		return Response.ok(res).build();
	}

	@Override
	@POST
	@Path("/get-excel-content")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response getExcelContent(@Context HttpServletRequest req) {
		String agentId = (String) req.getSession().getAttribute("agentid");
//		Boolean isLoggedIn = TcbsUtils.validateLogin(req);
//		if (!isLoggedIn) {
//			ResponseData<String> res = new ResponseData<String>(1, null,
//					"NOT_AUTHORIZED");
//			return Response.ok(res).build();
//		} else {
//			List<String> excels = (List) req.getSession().getAttribute(
//					"excelfile");
			List<String> excels = new ArrayList<String>();
			excels.add("/Users/kiennt/Documents/TCBS-OpenAccount.xlsx");
			
			if(excels.isEmpty()){
				ResponseData<String> res = new ResponseData<String>(1, null,
						"NO_DATA");
				return Response.ok(res).build();
			}
			String filePath = excels.get(0);

			List<String[]> persons = openAccountService.readExcelFile(filePath);
			List<OpenAccountMessage> result = new ArrayList<OpenAccountMessage>();
			String[] person;
			for (int j = 0; j < persons.size(); j++) {
				person = persons.get(j);
				OpenAccountMessage msg = new OpenAccountMessage();
				msg.setLastname(person[1]);
				msg.setFirstname(person[2]);
				msg.setSex(person[3].equalsIgnoreCase("m") ? "001" : person[3]
						.equalsIgnoreCase("f") ? "002" : "003");
				msg.setDob(person[4]);
				msg.setIdnumber(person[5]);
				msg.setIddate(person[6]);
				msg.setIdplace(person[7]);
				msg.setEmail(person[8]);
				msg.setMobile(person[9]);
				msg.setAddress(person[10]);
				msg.setProvince(person[11]);

				result.add(msg);
			}
			ResponseData<List> res = new ResponseData<List>(0, result,
					"SUCCESS");
			return Response.ok(res).build();
//		}
	}

}