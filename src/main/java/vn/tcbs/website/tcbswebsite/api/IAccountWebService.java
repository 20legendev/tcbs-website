package vn.tcbs.website.tcbswebsite.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;

import vn.tcbs.website.tcbswebsite.object.OpenAccountMessage;

public interface IAccountWebService {
	public Response createNew(@Valid @RequestBody OpenAccountMessage msg,
			HttpServletRequest request);

	public Response checkIdNumber(@FormParam("idnumber") String idnumber,
			HttpServletRequest request);

	public Response cancelOpenAccount(@FormParam("idnumber") String idnumber,
			@Context HttpServletRequest request);

	Response openBatch(HttpServletRequest request);

	Response getExcelContent(HttpServletRequest req);
}
