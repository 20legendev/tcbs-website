package vn.tcbs.website.tcbswebsite.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the Contract database table.
 * 
 */
@Entity
@NamedQuery(name = "Contract.findAll", query = "SELECT c FROM Contract c")
public class Contract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private long id;

	@Column(name = "AgentId")
	private String agentId;

	@Column(name = "ContractNumber")
	private String contractNumber;

	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name = "DocumentAlias")
	private String documentAlias;

	@Column(name = "DocumentPath")
	private String documentPath;

	@Column(name = "DownloadCount")
	private long downloadCount;

	@Column(name = "IdNumber")
	private String idNumber;

	public Contract() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAgentId() {
		return this.agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getContractNumber() {
		return this.contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDocumentAlias() {
		return this.documentAlias;
	}

	public void setDocumentAlias(String documentAlias) {
		this.documentAlias = documentAlias;
	}

	public String getDocumentPath() {
		return this.documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public long getDownloadCount() {
		return this.downloadCount;
	}

	public void setDownloadCount(long downloadCount) {
		this.downloadCount = downloadCount;
	}

	public String getIdNumber() {
		return this.idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

}