package vn.tcbs.website.tcbswebsite.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Province database table.
 * 
 */
@Entity
@NamedQuery(name="Province.findAll", query="SELECT p FROM Province p")
public class Province implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="Name")
	private String name;

	@Column(name="Path")
	private String path;

	@Column(name="ProvinceOrder", nullable=true)
	private Integer provinceOrder;

	public Province() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getProvinceOrder() {
		return this.provinceOrder;
	}

	public void setProvinceOrder(Integer provinceOrder) {
		this.provinceOrder = provinceOrder;
	}

}