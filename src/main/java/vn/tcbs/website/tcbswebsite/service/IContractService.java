package vn.tcbs.website.tcbswebsite.service;

import vn.tcbs.website.tcbswebsite.entity.Contract;

public interface IContractService {
	public Contract addNew(Contract c);

	public Contract getByAlias(String alias);

	public Contract getByIdNunber(String idNumber);

	public Contract updateByIdNumber(Contract c);

	public void update(Contract c);
}
