package vn.tcbs.website.tcbswebsite.service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.website.tcbswebsite.dao.IOpenAccountRequestDAO;
import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;

@Service("openAccountService")
public class OpenAccountService implements IOpenAccountService {

	@Autowired
	private IOpenAccountRequestDAO openAccountDAO;

	@Override
	public void addNew(String fullName, String idNumber, String agentId,
			String referenceId, String requestData) {
		openAccountDAO.addNew(fullName, idNumber, agentId, referenceId,
				requestData);
	}

	@Override
	public void addOrUpdateByIdNumber(String fullName, String idNumber,
			String agentId, String referenceId, String requestData) {
		List<OpenAccountRequest> data = openAccountDAO.find(agentId, idNumber, 0);
		if(data.size() > 0){
			OpenAccountRequest old = data.get(0);
			OpenAccountRequest acc = new OpenAccountRequest();
			acc.setFullName(fullName);
			acc.setAgentId(agentId);
			acc.setRequestData(requestData);
			acc.setLastUpdated(new Date());
			acc.setId(old.getId());
			openAccountDAO.update(acc);
		}else{
			this.addNew(fullName, idNumber, agentId, referenceId, requestData);
		}
	}
	
	@Override
	public OpenAccountRequest getByIdNumberAndAgentAndStatus(String idnumber,
			String agentId, Integer status) {
		List<OpenAccountRequest> result = openAccountDAO
				.getByIdNumberAndAgentAndStatus(agentId, idnumber, status);
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public void changeStatus(OpenAccountRequest obj, Integer status) {
		openAccountDAO.changeStatus(obj, status);
	}

	@Override
	public List<OpenAccountRequest> find(String agentId, String idNumber,
			Integer status) {
		return openAccountDAO.find(agentId, idNumber, status);
	}

	public List<String[]> readExcelFile(String filePath) {
		List<String[]> persons = new ArrayList<String[]>();
		try {
			Workbook wb;
			try {
				wb = new HSSFWorkbook(new FileInputStream(filePath));
			} catch (Exception ex) {
				wb = new XSSFWorkbook(new FileInputStream(filePath));
			}
			Sheet sheet = wb.getSheetAt(0);
			Row row;
			Cell cell;
			int rows;
			rows = sheet.getPhysicalNumberOfRows();
			int cols = 0;
			int tmp = 0;
			for (int i = 0; i < 10 || i < rows; i++) {
				row = sheet.getRow(i);
				if (row != null) {
					tmp = sheet.getRow(i).getPhysicalNumberOfCells();
					if (tmp > cols)
						cols = tmp;
				}
			}
			String[] person = new String[12];
			for (int r = 1; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					for (int c = 0; c < cols; c++) {
						cell = row.getCell((short) c);
						if (cell != null) {
							switch (cell.getCellType()) {
							case 0:
								person[c] = String.valueOf(cell
										.getNumericCellValue());
								break;
							case 1:
								person[c] = cell.getStringCellValue();
								break;
							}
						}
					}
					persons.add(person);
					person = new String[12];
				}
			}
			return persons;
		} catch (Exception ex) {
			return persons;
		}
	}

}
