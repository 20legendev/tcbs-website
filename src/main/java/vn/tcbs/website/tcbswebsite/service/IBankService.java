package vn.tcbs.website.tcbswebsite.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import vn.tcbs.website.tcbswebsite.object.Crbbanklist;

public interface IBankService {
	public List<Crbbanklist> listAll(String province) throws ClientProtocolException, IOException;
	public HashMap<String, Object> getByBankCode(String bankCode);
}
