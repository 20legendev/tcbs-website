package vn.tcbs.website.tcbswebsite.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.website.tcbswebsite.dao.IProvinceDAO;
import vn.tcbs.website.tcbswebsite.entity.Province;

@Service("provinceService")
public class ProvinceService implements IProvinceService {

	@Autowired
	private IProvinceDAO provinceDAO;

	@Override
	public List<Province> listAll() {
		return provinceDAO.listAll();
	}

	@Override
	public Province getByPath(String path) {
		List<Province> result = provinceDAO.getByPath(path);
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

}
