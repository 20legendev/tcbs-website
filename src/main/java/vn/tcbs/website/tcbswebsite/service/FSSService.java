package vn.tcbs.website.tcbswebsite.service;

import java.util.HashMap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

import vn.tcbs.tool.base.TcbsSoapUtils;
import vn.tcbs.tool.base.TcbsUtils;
import vn.tcbs.website.tcbswebsite.entity.Contract;
import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;
import vn.tcbs.website.tcbswebsite.object.OpenAccountMessage;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service("fssService")
public class FSSService implements IFSSService {

	@Autowired
	private IOpenAccountService openAccountService;
	@Autowired
	private IContractService contractService;

	@Value("${fss.wsdl.uri}")
	private String serverURI;
	@Value("${fss.wsdl.location}")
	private String service;
	@Value("${fss.soapfile.accountopenconfirm}")
	private String openAccountFile;
	@Value("${fss.soapfile.accountopencancel}")
	private String openAccountCancelFile;
	@Value("${fss.wsdl.action}")
	private String fssAction;

	@Override
	public Integer openAccount(OpenAccountMessage msg, String agentId) {
		SOAPConnection soapConnection = TcbsSoapUtils.getSoapConnection();
		Integer res = -1;
		if (soapConnection != null) {
			try {
				// Create SOAP Request message
				String serverURI = "http://tempuri.org/";
				String fullName = msg.getLastname() + " " + msg.getFirstname();
				String referenceId = TcbsUtils.generateRandom(12);

				HashMap<String, Object> data = new HashMap<String, Object>();
				data.put("funcname", "OPENACCOUNT");
				data.put("routecode", "Flex");
				data.put("module", "AccountService");
				data.put("referenceid", referenceId);
				data.put("fullname", fullName);
				data.put("sex", msg.getSex());
				data.put("idnumber", msg.getIdnumber());
				data.put("idtype", "I");
				data.put("iddate", msg.getIddate());
				data.put("idplace", msg.getIdplace());
				data.put("email", msg.getEmail());
				data.put("mobile", msg.getMobile());
				data.put("address", msg.getAddress());
				data.put("province", msg.getProvince());
				data.put("dob", msg.getDob());

				if (msg.getIsbank() == 1) {
					data.put("bankaccountnumber", msg.getBankaccountnumber());
					data.put("bankaccountname", msg.getBankaccountname());
					data.put("bankname", msg.getBankname());
					data.put("bankbranch", msg.getBankbranch());
					data.put("bankcode", msg.getBankcode());
				} else {
					data.put("bankaccountnumber", "");
					data.put("bankaccountname", "");
					data.put("bankname", "");
					data.put("bankbranch", "");
					data.put("bankcode", "");
				}
				data.put("custodycode", "11111111");
				data.put("agentid", agentId);

				SOAPMessage req = TcbsSoapUtils.generateSoapMessage(
						"soap/account-open-request.xml", data, serverURI
								+ "IOnlineTradingWcf/FlexService");
				SOAPMessage soapResponse = soapConnection.call(req,
						this.service);

				soapConnection.close();
				SOAPBody body = soapResponse.getSOAPBody();

				NodeList nodes = body.getElementsByTagName("FlexServiceResult");
				String bodyText = nodes.item(0).getFirstChild().getNodeValue();

				HashMap<String, String> result = new ObjectMapper().readValue(
						bodyText, HashMap.class);

				Integer errorCode = result.containsKey("p_err_code") ? Integer
						.valueOf(result.get("p_err_code")) : Integer
						.valueOf(result.get("P_ERR_CODE"));
				switch (errorCode) {
				case 0:
					openAccountService.addOrUpdateByIdNumber(fullName,
							msg.getIdnumber(), agentId,
							String.valueOf(referenceId),
							new ObjectMapper().writeValueAsString(data));
					Contract c = new Contract();
					c.setAgentId(agentId);
					c.setIdNumber(msg.getIdnumber());
					contractService.addNew(c);
					res = 0;
					break;
				default:
					res = errorCode;
					break;
				}
			} catch (Exception ex) {
				res = 1;
			}
		}
		return res;
	}

	@Override
	public Boolean confirmOpeningAccount(String idnumber, String agentid) {
		OpenAccountRequest acc = openAccountService
				.getByIdNumberAndAgentAndStatus(idnumber, agentid, 0);
		if (acc == null) {
			return false;
		}
		String referenceId = acc.getReferenceId();
		SOAPConnection soapConnection = TcbsSoapUtils.getSoapConnection();
		if (soapConnection != null) {
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("referenceid", referenceId);

			SOAPMessage soapMessage = TcbsSoapUtils.generateSoapMessage(
					openAccountFile, data, this.serverURI + fssAction);
			SOAPMessage soapResponse;
			try {
				soapResponse = soapConnection.call(soapMessage, this.service);
				soapConnection.close();
				SOAPBody body = soapResponse.getSOAPBody();

				NodeList nodes = body.getElementsByTagName("FlexServiceResult");
				String bodyText = nodes.item(0).getFirstChild().getNodeValue();
				System.out.println("RETURN " + bodyText);
				try {
					HashMap<String, Object> result = new ObjectMapper()
							.readValue(bodyText, HashMap.class);
					if (result.containsKey("p_err_code")) {
						String errorCode = String.valueOf(result
								.get("p_err_code"));
						if (errorCode.equals("0")) {
							return true;
						}
						return false;
					}
					return false;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			} catch (SOAPException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	@Override
	public boolean cancelOpenAccount(String referenceId) {
		// TODO Auto-generated method stub
		SOAPConnection soapConnection = TcbsSoapUtils.getSoapConnection();
		if (soapConnection != null) {
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("referenceid", referenceId);

			SOAPMessage soapMessage = TcbsSoapUtils.generateSoapMessage(
					openAccountCancelFile, data, this.serverURI + fssAction);
			SOAPMessage soapResponse;
			try {
				soapResponse = soapConnection.call(soapMessage, this.service);
				soapConnection.close();
				SOAPBody body = soapResponse.getSOAPBody();
				NodeList nodes = body.getElementsByTagName("FlexServiceResult");
				String bodyText = nodes.item(0).getFirstChild().getNodeValue();
				System.out.println("RETURN " + bodyText);
				try {
					HashMap<String, Object> result = new ObjectMapper()
							.readValue(bodyText, HashMap.class);
					if (result.containsKey("p_err_code")) {
						String errorCode = String.valueOf(result
								.get("p_err_code"));
						if (errorCode.equals("0")) {
							return true;
						}
						return false;
					}
					return false;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			} catch (SOAPException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

}
