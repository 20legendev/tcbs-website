package vn.tcbs.website.tcbswebsite.service;

import java.util.List;

import vn.tcbs.website.tcbswebsite.entity.OpenAccountRequest;

public interface IOpenAccountService {
	public void addNew(String fullName, String idNumber, String agentId,
			String referenceId, String requestData);
	public void addOrUpdateByIdNumber(String fullName, String idNumber, String agentId,
			String referenceId, String requestData);

	public OpenAccountRequest getByIdNumberAndAgentAndStatus(String idnumber,
			String agentid, Integer status);
	public void changeStatus(OpenAccountRequest obj, Integer status);
	public List<OpenAccountRequest>  find(String agentId, String idNumber, Integer status);
	public List<String[]> readExcelFile(String filePath);
}
