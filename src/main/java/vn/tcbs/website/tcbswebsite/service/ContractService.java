package vn.tcbs.website.tcbswebsite.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.tool.base.TcbsUtils;
import vn.tcbs.website.tcbswebsite.dao.IContractDAO;
import vn.tcbs.website.tcbswebsite.entity.Contract;

@Service("contractService")
public class ContractService implements IContractService {

	@Autowired
	private IContractDAO contractDAO;

	@Override
	public Contract addNew(Contract c) {
		return contractDAO.createNew(c);
	}

	@Override
	public Contract getByAlias(String alias) {
		List<Contract> result = contractDAO.getByAlias(alias);
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Contract getByIdNunber(String idNumber) {
		List<Contract> result = contractDAO.getByIdNumber(idNumber);
		return result.size() > 0 ? result.get(0) : null;
	}

	@Override
	public Contract updateByIdNumber(Contract c) {
		Contract old = this.getByIdNunber(c.getIdNumber());
		if (old != null) {
			c.setId(old.getId());
			c.setContractNumber(old.getContractNumber());
			c.setDocumentAlias(old.getDocumentAlias());
			c.setCreatedDate(new Date());
			contractDAO.update(c);
			return c;
		} else {
			return this.addNew(c);
		}
	}

	public void update(Contract c) {
		contractDAO.update(c);
	}

}
