package vn.tcbs.website.tcbswebsite.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.website.tcbswebsite.object.Crbbanklist;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service("bankService")
public class BankService implements IBankService {

	@Value("${service.url.bankservice}")
	private String bankServiceUrl;

	@Override
	public List<Crbbanklist> listAll(String province)
			throws ClientProtocolException, IOException {
		HttpClient client = HttpClientBuilder.create().build();

		HttpGet req = new HttpGet(bankServiceUrl + "bank/by-province/"
				+ province);
		HttpResponse response = client.execute(req);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		ObjectMapper mapper = new ObjectMapper();
		ResponseData<List<Crbbanklist>> banks = mapper.readValue(
				result.toString(), ResponseData.class);
		if (banks.getStatus() == 0) {
			return banks.getData();
		}
		return null;
	}

	@Override
	public HashMap<String, Object> getByBankCode(String bankCode) {
		String url = bankServiceUrl + "bank/by-code/" + bankCode;
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet req = new HttpGet(url);
		HttpResponse response;
		try {
			response = client.execute(req);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			ObjectMapper mapper = new ObjectMapper();
			ResponseData<HashMap> banks = mapper.readValue(result.toString(),
					ResponseData.class);
			if (banks.getStatus() == 0) {
				return banks.getData();
			}
		} catch (IOException e) {
			return null;
		}
		return null;
	}
}
