package vn.tcbs.website.tcbswebsite.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import vn.tcbs.website.tcbswebsite.entity.Province;

public interface IProvinceService {
	public List<Province> listAll();
	public Province getByPath(String path);
}
