package vn.tcbs.website.tcbswebsite.service;

import vn.tcbs.website.tcbswebsite.object.OpenAccountMessage;

public interface IFSSService {
	public Boolean confirmOpeningAccount(String idnumber, String agentid);
	public boolean cancelOpenAccount(String referenceId);
	Integer openAccount(OpenAccountMessage msg, String agentId);
	
}
