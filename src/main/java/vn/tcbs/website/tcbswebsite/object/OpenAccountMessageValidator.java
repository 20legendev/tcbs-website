package vn.tcbs.website.tcbswebsite.object;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import vn.tcbs.tool.base.GoogleRecatpcha;
import vn.tcbs.tool.base.TcbsUtils;

public class OpenAccountMessageValidator implements Validator {

	@Override
	public boolean supports(Class clazz) {
		return OpenAccountMessage.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		OpenAccountMessage msg = (OpenAccountMessage) target;

		validateEmpty(msg.getLastname(), "lastname", errors);
		validateSpecialCharacter(msg.getLastname(), "lastname", errors);
		
		validateSpecialCharacter(msg.getFirstname(), "firstname", errors);
		validateEmpty(msg.getFirstname(), "firstname", errors);
		
		validateEmpty(msg.getIdnumber(), "idnumber", errors);
		validateSpecialCharacter(msg.getIdplace(), "idplace", errors);
		validateEmpty(msg.getIdplace(), "idplace", errors);

		validateSpecialCharacter(msg.getAddress(), "address", errors);
		validateEmpty(msg.getAddress(), "address", errors);

		if (TcbsUtils.validatePhone(msg.getMobile()) == false) {
			errors.rejectValue("mobile", "Wrong format");
		}
		if (TcbsUtils.validateEmail(msg.getEmail()) == false) {
			errors.rejectValue("email", "Email wrong format");
		}
		validateEmpty(msg.getProvince(), "province", errors);
		if (msg.getProvince().equals("-1")) {
			errors.rejectValue("province", "Province is blank");
		}
		if (TcbsUtils.validateDate(msg.getIddate(), "dd/mm/yyyy") == false) {
			errors.rejectValue("iddate", "Wrong format");
		}
		if(msg.getIsbank() == 1){
			validateEmpty(msg.getBankaccountnumber(), "bankaccountnumber", errors);
			validateEmpty(msg.getBankaccountname(), "bankaccountname", errors);
			validateEmpty(msg.getBankname(), "bankname", errors);
			validateEmpty(msg.getBankcode(), "bankcode", errors);
			if(msg.getBankname().equals("-1")){
				errors.rejectValue("bankname", "Wrong format");
			}
			if(msg.getBankcode().equals("-1")){
				errors.rejectValue("bankcode", "Wrong format");
			}
		}
		if(msg.getSex().equals(null) || msg.getSex().equals("")){
			errors.rejectValue("sex", "Missing value");
		}
	}

	private void validateEmpty(String s, String val, Errors errors) {
		if (s.trim().isEmpty()) {
			errors.rejectValue(val, "Can't be empty");
		}
	}

	private void validateSpecialCharacter(String s, String val, Errors errors) {
		Pattern p = Pattern.compile("[$&+:;=?@#|'\\\\]");
		Matcher m = p.matcher(s);
		boolean b = m.find();
		if (b) {
			errors.rejectValue(val, "Name can't have special characters");
		}
	}

}
