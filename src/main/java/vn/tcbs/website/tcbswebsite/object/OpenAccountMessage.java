package vn.tcbs.website.tcbswebsite.object;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import vn.tcbs.tool.base.TcbsUtils;

public class OpenAccountMessage{
	private String funcname;
	private String routecode;
	private String module;
	private String referenceid;
	@NotNull
	private String firstname;
	@NotNull
	private String lastname;
	@NotNull
	private String sex;
	@NotNull
	private String idnumber;
	@NotNull
	private String idtype;
	@NotNull
	private String iddate;
	@NotNull
	private String idplace;
	@NotNull
	private String email;
	@NotNull
	private String mobile;
	@NotNull
	private String address;
	@NotNull
	private String province;
	
	private int isbank;
	
	private String bankaccountnumber;
	private String bankaccountname;
	private String bankname;
	private String bankbranch;
	private String bankcode;
	private String custodycode;
	private String agentid;
	private String captcha;
	
	private String dob;

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFuncname() {
		return funcname;
	}

	public void setFuncname(String funcname) {
		this.funcname = funcname;
	}

	public String getRoutecode() {
		return routecode;
	}

	public void setRoutecode(String routecode) {
		this.routecode = routecode;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getReferenceid() {
		return referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getIdtype() {
		return idtype;
	}

	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}

	public String getIddate() {
		return iddate;
	}

	public void setIddate(String iddate) {
		this.iddate = iddate;
	}

	public String getIdplace() {
		return idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getBankaccountnumber() {
		return bankaccountnumber;
	}

	public void setBankaccountnumber(String bankaccountnumber) {
		this.bankaccountnumber = bankaccountnumber;
	}

	public String getBankaccountname() {
		return bankaccountname;
	}

	public void setBankaccountname(String bankaccountname) {
		this.bankaccountname = bankaccountname;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getBankbranch() {
		return bankbranch;
	}

	public void setBankbranch(String bankbranch) {
		this.bankbranch = bankbranch;
	}

	public String getBankcode() {
		return bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getCustodycode() {
		return custodycode;
	}

	public void setCustodycode(String custodycode) {
		this.custodycode = custodycode;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public int getIsbank() {
		return isbank;
	}

	public void setIsbank(int isbank) {
		this.isbank = isbank;
	}
}
