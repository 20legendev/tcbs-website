package vn.tcbs.website.tcbswebsite.object;

import java.io.Serializable;
import java.sql.Timestamp;

public class Crbbanklist implements Serializable {
	private String bankcode;

	private String bankname;

	private String banksys;

	private String branchcode;

	private String branchname;

	private Timestamp createdt;

	private String regional;

	private String status;

	public Crbbanklist() {
	}

	public String getBankcode() {
		return this.bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getBankname() {
		return this.bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getBanksys() {
		return this.banksys;
	}

	public void setBanksys(String banksys) {
		this.banksys = banksys;
	}

	public String getBranchcode() {
		return this.branchcode;
	}

	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}

	public String getBranchname() {
		return this.branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public Timestamp getCreatedt() {
		return this.createdt;
	}

	public void setCreatedt(Timestamp createdt) {
		this.createdt = createdt;
	}

	public String getRegional() {
		return this.regional;
	}

	public void setRegional(String regional) {
		this.regional = regional;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}