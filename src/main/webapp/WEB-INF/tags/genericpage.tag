<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="script" fragment="true"%>
<%@attribute name="css" fragment="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />
<base href="${pageContext.request.contextPath}/" />

<title>Techcom Securities</title>

<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/addmore.css" rel="stylesheet">
<link href="css/additional.css" rel="stylesheet">
<link rel="stylesheet" media="screen and (max-width: 560px)"
	href="css/480.css">

<jsp:invoke fragment="css" />

<!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">

</head>
<body class="homepage">
	<div id="pageheader">
		<jsp:include page="/WEB-INF/jsp/includes/top.jsp"></jsp:include>
		<jsp:invoke fragment="header" />
	</div>
	<div id="body">
		<jsp:include page="/WEB-INF/jsp/includes/breadcrumb.jsp"></jsp:include>
		<jsp:doBody />
	</div>
	<div id="pagefooter">
		<div class="loading" style="background-image: url(img/loader.gif)"></div>
		<div>
			<jsp:include page="/WEB-INF/jsp/includes/footer.jsp"></jsp:include>
		</div>

		<script src="js/library/jquery.js"></script>
		<script src="js/library/bootstrap.min.js"></script>
		<script src="js/library/bootstrap-datepicker.min.js"></script>
		<script src="js/library/bootstrap-carousel.js"></script>
		<script src="js/library/jquery.prettyPhoto.js"></script>
		<script src="js/library/jquery.isotope.min.js"></script>
		<script src="js/main.js"></script>
		<script src="js/library/wow.min.js"></script>
		<script src="js/library/ejs_production.js"></script>


		<jsp:invoke fragment="script" />
</body>
</html>