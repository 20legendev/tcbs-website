<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="path-page">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="#">Trang chủ</a></li>
			<li class="active">Thông tin tài khoản</li>
			<div class="account-info">
				<c:if test="${not empty agentid}">
				    <a class="back-btn" href="tai-khoan.html">Bảng điều khiển</a>
					<span class="agent"><b>${agentid}</b></span>
					<a href="rm/logout.html">Thoát</a>
				</c:if>
			</div>
		</ol>
	</div>
</div>