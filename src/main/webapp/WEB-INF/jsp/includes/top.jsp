<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
    language="java"%>
<header id="header">
	<div class="top-bar">
		<div class="container">
			<div class="row">
				<div class="top-number btn-icon btn-phone">
					<p>
						<i></i> +844 2220 1039
					</p>
				</div>
				<ul class="navbar-sub">
					<li><a style="color: red;" id="myCustomTrigger"
						href="javascript:void(0);">Feedback</a></li>
				</ul>
				<div class="social">
					<form class="navbar-form" role="search">
						<div class="form-group">
							<button id="loginBtn" class="btn btn-default btn-login btn-icon">
								<i></i>Login
							</button>
						</div>
						<div class="form-group">
							<div class="form-search">
								<i class="fa fa-search ic-search"></i> <input type="text"
									class="form-control" placeholder="Search">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-inverse" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<ul class="nav navbar-nav">
					<li class="dropdown"><a class="navbar-brand"
						data-toggle="dropdown" href="index.html"><img
							src="images/Teckcomsecurity.png" alt="logo"></a>
						<ul class="dropdown-menu">
							<li><a href="javascript:showIntro();">Giới thiệu</a></li>
							<li><a href="#">Lịch sử</a></li>
							<li><a href="#">Đổi ngũ</a></li>
						</ul></li>
					<ul>
			</div>
			<div class="collapse navbar-collapse navbar-right">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a href="index.html"
						class="dropdown-toggle" data-toggle="dropdown">Giao dịch <i
							class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="#">Môi giới online</a></li>
							<li><a href="#">Dịch vụ lưu ký CK</a></li>
							<li><a href="#">Dịch vụ tài chính</a></li>
							<li><a href="#">Phân phối SP đầu tư</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Sản phẩm <i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="#">Trái phiếu TCBond</a></li>
							<li><a href="#">Chứng chỉ quỹ</a></li>
							<li><a href="#">Sản phẩm cấu trúc</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Tài chính doanh nghiệp <i
							class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="#">Tư vấn huy động vốn</a></li>
							<li><a href="#">Mua bán sát nhập</a></li>
							<li><a href="#">Tư vấn cổ phần hóa</a></li>
							<li><a href="#">Công cụ tài chính</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Hỗ trợ <i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="#">Thông tin thị trường</a></li>
							<li><a href="#">Nghiên cứu phân tích</a></li>
							<li><a href="javascript:showPdf();">Hướng dẫn mở tài
									khoản</a></li>
							<li><a href="javascript:showHelpPdf();">Hướng dẫn sử
									dụng</a></li>
						</ul></li>
				</ul>
				<div class="open-account">
					<a href="mo-tai-khoan.html" class="btn-icon btn-open"><i></i>Mở
						tài khoản</a>
				</div>
			</div>
		</div>
	</nav>
</header>