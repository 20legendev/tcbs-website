<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	language="java"%>
<div id="bottom">
	<div class="container wow fadeInDown" data-wow-duration="1000ms"
		data-wow-delay="600ms">
		<div class="row">
			<div class="col-md-9 col-sm-6">
				<div class="col-md-2 col-sm-6">
					<div class="widget">
						<h3>Giới thiệu</h3>
						<ul>
							<li><a href="#">Giới thiệu</a></li>
							<li><a href="#">Lịch sử</a></li>
							<li><a href="#">Đội ngũ</a></li>
						</ul>
					</div>
				</div>
				<!--/.col-md-3-->

				<div class="col-md-2 col-sm-6">
					<div class="widget">
						<h3>Giao dịch</h3>
						<ul>
							<li><a href="#">Cổ phiếu</a></li>
							<li><a href="#">Tiền</a></li>
							<li><a href="#">Quỹ</a></li>
							<li><a href="#">Trái phiếu</a></li>
						</ul>
					</div>
				</div>
				<!--/.col-md-3-->

				<div class="col-md-2 col-sm-6">
					<div class="widget">
						<h3>Đầu tư</h3>
						<ul>
							<li><a href="#">Bond</a></li>
							<li><a href="#">Funds</a></li>
							<li><a href="#">Stock</a></li>
							<li><a href="#">ETF</a></li>
							<li><a href="#">Index</a></li>
						</ul>
					</div>
				</div>
				<!--/.col-md-3-->

				<div class="col-md-2 col-sm-6">
					<div class="widget">
						<h3>Tư vấn</h3>
						<ul>
							<li><a href="#">Tư vấn đầu tư cá nhân</a></li>
							<li><a href="#">Tài chính doanh nghiệp</a></li>
						</ul>
					</div>
				</div>
				<!--/.col-md-3-->

				<div class="col-md-2 col-sm-6">
					<div class="widget">
						<h3>Hỗ trợ</h3>
						<ul>
							<li><a href="#">Thông tin thị trường</a></li>
							<li><a href="#">Báo cáo phân tích</a></li>
						</ul>
					</div>
				</div>
				<!--/.col-md-3-->
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="widget-support">
					<div class="btn-icon title-support">
						<i></i>Newsletter
					</div>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Email">
						</div>
						<button type="submit" class="btn btn-default">Send</button>
					</form>
					<div class="widget-contact btn-icon">
						<i></i>
						<h6>CONTACT AND CHAT</h6>
						<p>Phone: +1.800.633.0738</p>
					</div>
				</div>
			</div>
			<!--/.col-md-3-->
		</div>
	</div>
</div>
<footer id="footer" class="midnight-blue">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<b>© Techcombank 2014</b><br /> <a href="#">Subscribe</a> | <a
					href="#">Careers</a> | <a href="#">Contact Us</a> | <a href="#">Site
					Maps</a> | <a href="#">Legal Notices</a> | <a href="#">Terms of Use</a>
				| <a href="#">Privacy</a> | <a href="#">Cookie Preferences</a> | <a
					href="#">Techcombank Mobile</a>
			</div>
			<div class="col-sm-4">
				<div class="pull-right">
					Kết nối với chúng tôi:
					<ul class="social-share">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
				</div>
			</div>
		</div>
	</div>
</footer>