<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="css">
    </jsp:attribute>
	<jsp:attribute name="script">
        <!-- app -->
        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
        <script src="js/Config.js"></script>
        <script src="js/main.js"></script>
        <script src="js/app/Utils.js"></script>
        <script src="js/app/Register.js"></script>
    </jsp:attribute>
	<jsp:body>
	    <div>
	        <div class="container">
	            <div class="infoTab row ">
	                <div class="col-xs-12 col-sm-12 col-md-12 ">
	                    <div class="blog-content">
	                        <div class="blog-info-acc">
		                        <div class="content-info-acc">
			                        <div class="content-info-acc-in">
			                           <div class="col-md-3 col-ms-3">
		                                      <div class="form-group">
		                                          <a href="mo-tai-khoan.html" class="btn btn-default btn2 dashboard-btn" type="submit">Mở tài khoản</a>
		                                      </div>
		                                </div>
		                                <div class="col-md-3 col-ms-3">
                                               <div class="form-group">
                                                   <a href="mo-tai-khoan-excel.html" class="btn btn-default btn2 dashboard-btn" type="submit">Mở sử dụng excel</a>
                                               </div>
                                         </div>
		                                <div class="col-md-3 col-ms-3">
                                              <div class="form-group">
                                                  <a href="tai-khoan/xac-nhan-mo-tai-khoan.html" class="btn btn-default btn2 dashboard-btn" type="submit">Xác nhận tài khoản</a>
                                              </div>
                                        </div>
                                        <div class="col-md-3 col-ms-3">
                                            <div class="form-group">
                                                  <a href="tai-khoan/danh-sach-khach-hang.html" class="btn btn-default btn2 dashboard-btn" type="submit">Quản lý khách hàng</a>
                                              </div>
                                        </div>
			                        </div>
		                        </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </jsp:body>
</t:genericpage>
