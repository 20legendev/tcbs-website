<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="css">
    </jsp:attribute>
	<jsp:attribute name="script">
        <!-- app -->
        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
        <script src="js/library/jquery.form.js"></script>
        <script src="js/Config.js"></script>
        <script src="js/main.js"></script>
        <script src="js/app/Utils.js"></script>
        <script src="js/app/Register.js"></script>
    </jsp:attribute>
	<jsp:body>
	    <div id="recent-news">
            <div class="container">
                <div class="infoTab row ">
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
	                   <div class="blog-info-acc">
	                       <div class="tab-info-acc">Đăng ký tài khoản trực tuyến</div>
	                       <div class="content-info-acc">
                                <div class="content-info-acc-in">
                                    <h5>
	                                   <img src="images/Icon Apps.png" />Mở nhiều tài khoản sử dụng file excel
                                    </h5>
	                                <div class="lineun"></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6 col-ms-6">
	                            <form id="uploadExcel" action="upload/excel.html" method="post" enctype="multipart/form-data">
	                                <input type="file" class="col-xs-9 excel-uploader" size="60" id="myfile" name="myfile" style="display:inline-block;"> 
									<input type="submit" value="Tải lên" class="tcbs-button">
	                            </form>
                            </div>
                            <div class="col-xs-6 col-md-6 col-ms-6">
                                Tải file mẫu tại đây 
                                <a href="file/public/TCBS-OpenAccount.xlsx.html">File mẫu</a>
                            </div>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Họ và tên</th>
                                        <th>CMND</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${persons}" var="person" varStatus="loop">
                                        <tr>
                                            <td>
                                                <c:out value="${loop.index + 1}" />
                                            </td>
                                            <td>
                                                <c:out value="${person[1]}" />
                                                <c:out value="${person[2]}" />
                                            </td>
                                            <td>
                                                <c:out value="${person[5]}" />
                                            </td>
                                            <td>
                                                <c:choose>
	                                                <c:when test="${errors.containsKey(person[5]) == true}">
		                                                <span class="label label-danger">error</span>
		                                            </c:when>
		                                            <c:otherwise>
		                                                <span class="label label-success">success</span>
		                                            </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	</jsp:body>
</t:genericpage>