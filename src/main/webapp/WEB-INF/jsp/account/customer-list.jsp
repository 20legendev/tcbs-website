<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="css">
    </jsp:attribute>
	<jsp:attribute name="script">
        <!-- app -->
        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
        <script src="js/Config.js"></script>
        <script src="js/main.js"></script>
        <script src="js/app/Utils.js"></script>
        <script src="js/app/Register.js"></script>
    </jsp:attribute>
	<jsp:body>
        <div id="recent-news">
	        <div class="container">
	            <div class="infoTab row ">
	                <div class="col-xs-12 col-sm-12 col-md-12 ">
	                    <div class="blog-content">
	                        <div class="blog-info-acc">
	                           <!--  Đăng ký mở tài khoản chứng khoán TechcomSecurities thành công! </br>
	                            <a
									href="tai-khoan/xac-nhan-mo-tai-khoan.html">Đến trang xác nhận mở tài khoản</a> -->
	                            <div class="content-info-acc">
		                            <div class="content-info-acc-in">
                                        <div class="col-xs-12 col-sm-12 col-md-12 account-filter-bar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    <c:choose>
                                                        <c:when test="${status == 0}">
                                                            <c:out value="Đang chờ xác nhận" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:out value="Đã mở thành công" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
													<li><a href="tai-khoan/danh-sach-khach-hang.html">Đang chờ xác nhận</a></li>
													<li><a href="tai-khoan/danh-sach-khach-hang.html?status=1">Đã mở thành công</a></li>
                                                </ul>
                                            </div>
                                        </div>
		                                  <table class="table">
                                               <thead>
                                                   <tr>
                                                       <th>STT</th>
                                                       <th>Họ và Tên</th>
                                                       <th>CMND</th>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                                   <c:forEach
													items="${nonRegistered}" var="person" varStatus="loop">
                                                      <tr
														onclick="Register.showOption($(this))">
                                                          <td>${loop.index + 1}</td>
                                                          <td>${person.getFullName()}</td>
                                                          <td>${person.getIdNumber()}</td>
                                                      </tr>
                                                   </c:forEach>
                                               </tbody>
                                           </table>
		                            
		                            
			                           <!--  <div style="width: 100%; display: block; overflow: hidden; clear: both;">
		                                     <div class="col-md-3 col-ms-3">
		                                         <div class="form-group">
		                                             <input class="btn btn-default btn2" type="submit" value="Đăng ký" onclick="Register.addAccount()">
		                                         </div>
		                                     </div>
		                                     <div class="col-md-3 col-ms-3">
		                                         <div class="form-group">
		                                             <input class="btn btn-default btn2" type="submit" value="Huỷ">
		                                         </div>
		                                     </div>
		                                     <div class="col-md-3 col-ms-3">
		                                         <div class="form-group">
		                                             <input class="btn btn-default btn2" type="submit" value="In hồ sơ">
		                                         </div>
		                                     </div>
		                                     <div class="col-md-3 col-ms-3">
		                                         <div class="form-group">
		                                             <input class="btn btn-default btn2" type="submit" value="Bổ sung thông tin">
		                                         </div>
		                                     </div>
		                                 </div>
	                                 </div> -->
                                 </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            
	            <script type="text/tmpl" id="actionBarWrapper">
                    <tr class="action-bar" id="actionBar">
                        <td colspan="3">
                            <div id="actionBar-cancel" class="button" onclick="Register.cancel($(this))" idnumber="">Huỷ</div>
                            <div class="button" onclick="Register.printIt()">In hồ sơ</div>
                            <a name="idopen" class="button">Xác nhận</a>
                        </td>
                    </tr>
                </script>
	            <a style="display: none" href="file/download/tc.html" target="_blank" id="tcDownload"></a>
	        </div>
	    </div>
        </div>
    </jsp:body>
</t:genericpage>