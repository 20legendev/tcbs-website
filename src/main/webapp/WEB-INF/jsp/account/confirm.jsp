<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
	<jsp:attribute name="css">
    </jsp:attribute>
	<jsp:attribute name="script">
        <!-- app -->
        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
        <script src="js/Config.js"></script>
        <script src="js/main.js"></script>
        <script src="js/app/Utils.js"></script>
        <script src="js/app/Register.js"></script>
    </jsp:attribute>
	<jsp:body>
	    <div id="recent-news">
	        <div class="container">
	            <div class="infoTab row ">
	                <div class="col-xs-12 col-sm-12 col-md-12 ">
	                    <div class="blog-content">
	                        <div class="blog-info-acc">
	                            <div class="tab-info-acc">Đăng ký tài khoản trực tuyến -
	                                Form cho cộng tác viên</div>
	                            <!---Box--->
	                            <div class="content-info-acc">
	                                <div class="content-info-acc-in">
	                                    <h5>
	                                        <img src="images/Icon Apps.png">Xác nhận hợp đồng
	                                    </h5>
	                                    <div class="lineun"></div>
	                                    <form class="col-md-12 col-ms-12 col-xs-12" method="POST">
	                                        <div class="col-md-3 col-ms-3">
	                                            <div class="form-group">
	                                                <label for="exampleInputEmail1">CMND khách hàng <span>*</span>
	                                                    :
	                                                </label> 
	                                                <input type="text" id="idnumber"
															class="form-control" placeholder="Số chứng minh nhân dân"
															name="idnumber" onkeyup="Register.checkIdNumber($(this))" value="${idopen}"/>
	                                            </div>
	                                            <c:choose>
	                                               <c:when test="${not empty user}">
	                                                   <div class="form-group" id="customerWrapper">
	                                                        Khách hàng: <b id="name">${user.getFullName()}</b> – Địa chỉ: <b
	                                                                id="address">${address}</b>
	                                                    </div>
	                                               </c:when>
	                                               <c:otherwise>
	                                                    <div class="form-group" id="customerWrapper" style="opacity: 0">
	                                                        Khách hàng: <b id="name"></b> – Địa chỉ: <b
	                                                                id="address"></b>
	                                                    </div>
	                                                </c:otherwise>
	                                            </c:choose>
                                                <div class="error-msg show">
                                                    <c:choose>
			                                            <c:when test="${error != null}">
			                                                ${error}
			                                            </c:when>
			                                        </c:choose>
                                                </div>
                                                <div class="form-group">
                                                    <input class="btn btn-default btn2" type="submit" value="Xác nhận">
                                                </div>
	                                        </div>
	                                        <div class="col-md-9">
    	                                        <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Họ và Tên</th>
												            <th>CMND</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
	                                                    <c:forEach items="${customer}" var="person">
	                                                       <tr onclick="Register.chooseConfirm('${person.getIdNumber()}')">
	                                                           <td></td>
	                                                           <td>${person.getFullName()}</td>
	                                                           <td>${person.getIdNumber()}</td>
	                                                       </tr>
	                                                    </c:forEach>
                                                    </tbody>
                                                </table>
	                                        </div>
	                                    </form>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </jsp:body>
</t:genericpage>
