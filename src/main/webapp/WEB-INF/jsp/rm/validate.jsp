<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="css">
    </jsp:attribute>
	<jsp:attribute name="script">
        <!-- app -->
	    <script src="js/Config.js"></script>
	    <script src="js/main.js"></script>
	    <script src="js/app/Utils.js"></script>
	    <script src="js/app/Register.js"></script>
    </jsp:attribute>
	<jsp:body>
	    <div id="recent-news">
	        <div class="container">
	            <div class="infoTab row ">
	                <div class="col-xs-12 col-sm-12 col-md-12 ">
	                    <div class="blog-content">
	                        <div class="blog-info-acc">
	                            <div class="tab-info-acc">Đăng ký tài khoản trực tuyến -
	                                Form cho cộng tác viên</div>
	                            <!---Box--->
	                            <form class="content-info-acc"
										action="rm/login.html" method="post">
	                                 <input type="hidden" name="redirect"
											value="${redirect}" />
	                                <div class="content-info-acc-in">
	                                    <h5>
	                                        <img src="images/Icon Apps.png">Thông tin cộng tác viên
	                                    </h5>
	                                    <div class="lineun"></div>
	
	                                    <!--------ColRespon-------->
	                                    <div
												class="col-md-12 col-ms-12 col-xs-12">
	                                        <div class="col-md-3 col-ms-3">
	                                            <div class="form-group">
	                                                <label
															for="exampleInputEmail1">Nhập mã cộng tác
	                                                    viên<span>*</span> :
	                                                </label> <input type="text"
															class="form-control" name="agentid"
															placeholder="Mã cộng tác viên">
	                                            </div>
	                                        </div>
	                                        <div
													class="col-md-12 col-ms-12 col-xs-12"></div>
	                                        <div class="col-md-3 col-ms-3">
	                                            <div class="form-group">
	                                                <input
															class="btn btn-default btn2" type="submit" value="Bắt đầu">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <!--------ColRespon-------->
	                                </div>
	                            </form>
	                            <!---Box-End--->
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!--/.row-->
	        </div>
	        <!--/.container-->
	    </div>
    </jsp:body>
</t:genericpage>