$(document).ready(function () {

    $('.btn-vertical-slider').on('click', function () {

        if ($(this).attr('data-slide') == 'next') {
            $('#myCarousel').carousel('next');
        }
        if ($(this).attr('data-slide') == 'prev') {
            $('#myCarousel').carousel('prev')
        }

    });
});

jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});


	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});	
});

$(document).ready(function() {

    // Switch Click
    $('.Switch').click(function() {

        // Check If Enabled (Has 'On' Class)
        if ($(this).hasClass('On')){

            // Try To Find Checkbox Within Parent Div, And Check It
            $(this).parent().find('input:checkbox').attr('checked', true);

            // Change Button Style - Remove On Class, Add Off Class
            $(this).removeClass('On').addClass('Off');
            $('.infoTab').removeClass('tabList')

        } else { // If Button Is Disabled (Has 'Off' Class)

            // Try To Find Checkbox Within Parent Div, And Uncheck It
            $(this).parent().find('input:checkbox').attr('checked', false);

            // Change Button Style - Remove Off Class, Add On Class
            $(this).removeClass('Off').addClass('On');

            $('.infoTab').addClass('tabList')
        }

    });

    // Loops Through Each Toggle Switch On Page
    $('.Switch').each(function() {

        // Search of a checkbox within the parent
        if ($(this).parent().find('input:checkbox').length){

            // This just hides all Toggle Switch Checkboxs
            // Uncomment line below to hide all checkbox's after Toggle Switch is Found
            //$(this).parent().find('input:checkbox').hide();

            // For Demo, Allow showing of checkbox's with the 'show' class
            // If checkbox doesnt have the show class then hide it
            if (!$(this).parent().find('input:checkbox').hasClass("show")){ $(this).parent().find('input:checkbox').hide(); }
            // Comment / Delete out the above line when using this on a real site

            // Look at the checkbox's checkked state
            if ($(this).parent().find('input:checkbox').is(':checked')){

                // Checkbox is not checked, Remove the 'On' Class and Add the 'Off' Class
                $(this).removeClass('On').addClass('Off');

            } else {

                // Checkbox Is Checked Remove 'Off' Class, and Add the 'On' Class
                $(this).removeClass('Off').addClass('On');

            }

        }

    });

});


// - Ignore End