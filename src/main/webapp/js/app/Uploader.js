Uploader = {
	init : function() {
		this.initUploader();
		console.log("Init uploader success");
	},

	initUploader : function() {
		var options = {
			beforeSend : function() {
				$("#progressbox").show();
				$("#progressbar").width('0%');
				$("#message").empty();
				$("#percent").html("0%");
			},
			uploadProgress : function(event, position, total, percentComplete) {
				$("#progressbar").width(percentComplete + '%');
				$("#percent").html(percentComplete + '%');
				if (percentComplete > 50) {
					$("#message")
							.html(
									"<font color='red'>File Upload is in progress</font>");
				}
			},
			success : function() {
				$("#progressbar").width('100%');
				$("#percent").html('100%');
				Register.getExcelData();
			},
			complete : function(response) {
				$("#message")
						.html(
								"<font color='blue'>Your file has been uploaded!</font>");
			},
			error : function() {
				$("#message")
						.html(
								"<font color='red'> ERROR: unable to upload files</font>");
			}
		};
		$("#uploadExcel").ajaxForm(options);
	}
};

$(document).ready(function() {
	Uploader.init();
});