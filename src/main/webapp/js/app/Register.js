Register = {

	init : function() {
		this.config = {
			check : {}
		};
		this.bindEvent();
	},

	showLoading : function(isShow) {
		if (isShow) {
			$('.loading').show();
		} else {
			$('.loading').hide();
		}
	},

	bindEvent : function() {
		var _self = this;
		$('input').on('keyup', function(e) {
			if (e.keyCode == 13) {
				_self.addAccount();
			}
		});
	},

	changeCheckbox : function(obj) {
		if (obj.prop('checked')) {
			var group = "input:checkbox[name='" + obj.attr("name") + "']";
			$(group).prop('checked', false);
			obj.prop('checked', true);
		} else {
			obj.prop('checked', false);
		}
	},

	changeGender : function(obj) {
		Register.changeCheckbox(obj);
		var gender = this.getGender();
		if (!gender) {
			this.showError("gender", true, "Giới tính bị để trống");
		} else {
			this.showError("gender", false);
		}
	},

	addAccount : function() {
		if (!$('#confirm').prop('checked')) {
			$('#confirm').parent().addClass('error-text');
		} else {
			$('#confirm').parent().removeClass('error-text');
		}
		this.showLoading(true);
		var val = {}, error = {};
		$('.error-msg').removeClass('show');
		error = this.getValue(
				[ 'lastname', 'firstname', 'dob', 'idnumber', 'iddate',
						'idplace', 'email', 'mobile', 'address', 'province' ],
				error, val);
		if (!error['dob']) {
			if (!Utils.validateSmallerDate(val['dob'])) {
				error['dob'] = "Ngày lớn hơn ngày hiện tại";
			}
		}

		val['sex'] = this.getGender();
		if (val['sex'] == null) {
			error['gender'] = "001";
		}
		val['captcha'] = $('#g-recaptcha-response').val();
		if (this.config.isbankaccount == undefined)
			this.config.isbankaccount = 1;
		val['isbank'] = this.config.isbankaccount;
		if (val['isbank']) {
			error = this.getValue([ 'bankaccountnumber', 'bankaccountname',
					'bankplace', 'bankname', 'bankcode' ], error, val);
		}

		if (!Utils.validateEmail(val["emailInput"])) {
			$('#emailInput').addClass('error');
		}
		if (Object.keys(error).length > 0) {
			this.renderError(error);
			this.showLoading(false);
			return;
		}
		this.sendRegister(val);
	},

	getValue : function(arr, error, val) {
		for (var i = 0; i < arr.length; i++) {
			obj = $('#' + arr[i]);
			val[arr[i]] = Utils.trim(obj.val());
			if (!val[arr[i]] || val[arr[i]].length == 0 || val[arr[i]] == -1) {
				error[arr[i]] = "001";
			}
		}
		return error;
	},

	sendRegister : function(obj) {
		var _self = this;
		$('#captcha').removeClass("show");
		$('.form-group input').removeClass('error');
		$('.form-group select').removeClass('error');

		$.ajax({
			url : 'api/account/open',
			data : obj,
			method : "POST",
			dataType : "json",
			success : function(data) {
				_self.showLoading(false);
				_self.checkRegisterError(data);
			},
			failure : function() {
				grecaptcha.reset();
				_self.showLoading(false);
			}
		})
	},

	checkRegisterError : function(data) {
		switch (data.status) {
		case 0:
			top.location.href = "tai-khoan/danh-sach-khach-hang.html";
			break;
		default:
			switch (data.msg) {
			case 'CAPTCHA_ERROR':
				$('#captcha').addClass("show");
				break;
			case 'ID_DUPLICATED':
				this.showError('idnumber', true,
						'Chứng minh thư đã được sử dụng');
				break;
			case 'NOT_AUTHORIZED':
				top.location.href = '/rm/logout.html';
				break;
			default:
				this.showErrorToUser(data.data);
				break;
			}
			grecaptcha.reset();
			$('#basicModal').modal();
			break;
		}
	},

	chooseConfirm : function(idNumber) {
		$('#idnumber').val(idNumber);
	},

	renderError : function(err) {
		var obj;
		for ( var key in err) {
			obj = $('.error-msg[for=' + key + ']');
			obj.addClass('show');
			var errMsg = err[key] != "001" ? err[key]
					: "Trống hoặc không đúng định dạng";
			obj.html(errMsg);
		}
		this.showLoading(false);
	},

	getGender : function() {
		var gender0 = $('#gender0').prop('checked');
		var gender1 = $('#gender1').prop('checked');
		var gender2 = $('#gender2').prop('checked');
		var gender = gender0 ? "000" : (gender1 ? "001" : gender2 ? "002"
				: null);
		return gender;
	},

	changeProvince : function(obj, holder) {
		this.validateProvince(obj);
		$('#branch').html('<option value=-1>Chọn chi nhánh</option>');
		this.showLoading(true);
		var _self = this;
		$
				.ajax({
					url : '/hostread/bank/by-province/' + obj.val(),
					success : function(data) {
						if (data.status == 0) {
							var bankWrapper = $('#bankname');
							bankWrapper.html('');
							bankWrapper
									.append("<option value=-1>Chọn ngân hàng</option>");
							for (var i = 0; i < data.data.length; i++) {
								bankWrapper.append("<option value='"
										+ data.data[i].banksys + "'>"
										+ data.data[i].bankname + "</option>");
							}
						}
						_self.showLoading(false);
					},
					failure : function() {
						_self.showLoading(false);
					}
				});
	},

	changeBank : function(obj) {
		this.showLoading(true);
		var _self = this;
		$.ajax({
			url : '/hostread/bank/branch/' + $('#bankplace').val() + '/'
					+ obj.val(),
			success : function(data) {
				if (data.status == 0) {
					var bankWrapper = $('#bankcode');
					bankWrapper
							.html('<option value=-1>Chọn chi nhánh</option>');
					for (var i = 0; i < data.data.length; i++) {
						bankWrapper.append("<option value='"
								+ data.data[i].bankcode + "'>"
								+ data.data[i].branchname + "</option>");
					}
				}
				_self.showLoading(false);
			},
			failure : function() {
				_self.showLoading(false);
			}
		});
		this.showError('bankname', false);
	},
	validateName : function(obj) {
		var val = obj.val();
		val = Utils.trim(val);
		if (val.length == 0 || Utils.validateSpecialCharacter(val)) {
			obj.addClass("error");
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validateEmail : function(obj) {
		var val = obj.val();
		if (!Utils.validateEmail(val)) {
			this.showError(obj.attr('id'), true, "Email không đúng định dạng ");
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validatePhone : function(obj) {
		var val = obj.val();
		if (!Utils.validatePhone(val)) {
			obj.addClass("error");
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validateDate : function(obj, year, text) {
		if (!Utils.validateSmallerDate(obj.val(), year)) {
			text = text ? text : "Ngày sai định dạng";
			this.showError(obj.attr('id'), true, text);
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validateProvince : function(obj) {
		var val = obj.val();
		console.log(val, obj.attr('id'));
		if (val == -1) {
			this.showError(obj.attr('id'), true);
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	changeMoneyInput : function(obj) {
		this.changeCheckbox(obj);
		this.config.isbankaccount = $('#isMoneyInput-1').prop('checked') ? 1
				: 0;
		if (this.config.isbankaccount) {
			$('.showbank').show();
		} else {
			$('.showbank').hide();
		}
	},

	confirm : function(obj) {
		if (obj.prop('checked')) {
			obj.parent().removeClass('error-text');
		}
	},

	showErrorToUser : function(data) {
		for (var i = 0; i < data.length; i++) {
			this.showError(data[i].field, true);
			$('#' + data[i].field).addClass('error');
		}
	},

	showError : function(id, isShow, text) {
		if (isShow) {
			$('#' + id).addClass('error');
			$('div[for=' + id + ']').addClass('show').html(
					text ? text : 'Không đúng định dạng');
		} else {
			$('#' + id).removeClass('error');
			$('div[for=' + id + ']').removeClass('show');
		}
	},

	checkIdNumber : function(obj) {
		var idnumber = obj.val();
		$.ajax({
			url : 'api/account/check-id',
			method : "POST",
			data : {
				idnumber : idnumber
			},
			success : function(data) {
				if (data.status == 0) {
					if (data.data != null) {
						$('#customerWrapper').css('opacity', '1');
						$('#name').html(data.data.fullName);
						var e = JSON.parse(data.data.requestData);
						$('#address').html(e.address);
					} else {
						$('#customerWrapper').css('opacity', '0');
					}
				}
			}
		})
	},

	showOption : function(obj) {
		$('#actionBar').remove();
		var html = $('#actionBarWrapper').html();
		obj.after(html);
		var id = obj.find('td:nth-child(3)').html();
		$('a[name=idopen]').attr("href",
				"tai-khoan/xac-nhan-mo-tai-khoan.html?idopen=" + id);
		$('#actionBar-cancel').attr('idnumber', id);
	},

	cancel : function(obj) {
		var _self = this;
		this.showLoading(true);
		$.ajax({
			url : "api/account/cancel",
			method : "post",
			data : {
				idnumber : obj.attr('idnumber')
			},
			success : function(data) {
				_self.showLoading(false);
				if (data.status == 0) {
					top.location.href = top.location.href;
				}
			},
			failure : function() {
				_self.showLoading(false);
			}
		})
	},

	printIt : function() {
		var _self = this;
		var idNumber = $("#actionBar-cancel").attr('idnumber');
		this.showLoading(true);
		$.ajax({
			url : "api/account/convert",
			method : "POST",
			data : {
				idnumber : idNumber
			},
			success : function(data) {
				_self.showLoading(false);
				if (data.status == 0) {
					top.location.href = "file/download/" + data.data + ".html";
					$('#tcDownload')[0].click();
				}
			},
			failure : function(err) {
				_self.showLoading(false);
			}
		})
	},
	
	getExcelData: function(){
		$.ajax({
			url: "api/account/get-excel-content",
			method: "POST",
			success: function(data){
				if(data.status == 0){
					console.log(data.data);
				}
			},
			failure: function(){
				
			}
		})
	}
};
$(document).ready(function() {
	Register.init();
});